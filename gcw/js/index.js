import * as THREE from '../three.js-master/build/three.module.js'
import { GLTFLoader } from '../three.js-master/examples/jsm/loaders/GLTFLoader.js'

var scene;
var camera;
var renderer;
var clock;
var deltaTime;
var keys = {};
var cube;
var cube2;
var cube3;
var plane;
var plane2;
var Nave = [];
var Disparos = [];
var DisparosN2 = [];
var Enemigos = [];
var Ambiental =[];
var Item = [];
var distanciacolision;
var comportamiento;
var kamikaze;
var movL1 = 20;
var movL2 = -20;
var lateral1;
var lateral2;



function image() {
    var image = new Image();
    image.src = IMAGE_SRC;
    image.onload = function () {
        WIDTH = image.width;
        HEIGHT = image.height;

        var canvas = document.createElement('canvas');
        canvas.width = WIDTH;
        canvas.height = HEIGHT;
        var context = canvas.getContext('2d');


        console.log('image loaded');
        context.drawImage(image, 0, 0);
        data = context.getImageData(0, 0, WIDTH, HEIGHT).data;

        console.log(data);

        init();
    }
}


const loader = new GLTFLoader()

$(document).ready(function () {
    setupScene();
    document.addEventListener('keydown', onKeyDown);
    document.addEventListener('keyup', onKeyUp);

    render();
});




function render() {
    requestAnimationFrame(render);
    deltaTime = clock.getDelta();

    var yaw = 0;
    var forward = 0;

    var yawN2 = 0;
    var forwardN2 = 0;

    if (keys["A"] &&  Nave[0].position.x <= 34) {

            yaw = 25;

        
    } else if (keys["D"] && Nave[0].position.x >= -35) {
        yaw = -25;
    }
    if (keys["W"] && Nave[0].position.z <= 20) {
        forward = 15;
    } else if (keys["S"] && Nave[0].position.z >= -19) {
        forward = -15;
    }

    //Movimientos Nave 2
    if (keys["J"] &&  Nave[1].position.x <= 34) {
        yawN2 = 25;
    } else if (keys["L"] && Nave[1].position.x >= -35) {
        yawN2 = -25;
    }
    if (keys["I"] && Nave[1].position.z <= 20) {
        forwardN2 = 15;
    } else if (keys["K"] && Nave[1].position.z >= -19) {
        forwardN2 = -15;
    }
    //Movimientos Nave 2

    for (var i = 0; i < Disparos.length; i++) {
        distanciacolision = cube.position.distanceToSquared(Disparos[i].position)
        if (distanciacolision <= 0.5) {
            scene.remove(Disparos[i]);
            console.log("colision")
        }
        if (Disparos[i] === undefined) continue;
        if (Disparos[i].alive == false) {
            Disparos.splice(i, 1);
            continue;
        }
        Disparos[i].position.add(Disparos[i].velocity);
    }


    if (keys["C"]) {
        var bullet = new THREE.Mesh(new THREE.SphereGeometry(0.3, 8, 8), new THREE.MeshBasicMaterial({ color: 0xffffff }));
        bullet.position.set(
            Nave[0].position.x,
            Nave[0].position.y,
            Nave[0].position.z + 3
        );
        bullet.velocity = new THREE.Vector3(
            -Math.sin(camera.rotation.y),
            0,
            Math.cos(camera.rotation.y)
        );
        bullet.alive = true;
        setTimeout(function () {
            bullet.alive = false;
            scene.remove(bullet);
        }, 1000);
        Disparos.push(bullet);
        scene.add(bullet);
    }


    //Disparos Nave 2
    for (var i = 0; i < DisparosN2.length; i++) {
        distanciacolision = cube.position.distanceToSquared(DisparosN2[i].position)
        if (distanciacolision <= 0.5) {
            scene.remove(Disparos[i]);
            console.log("colision")
        }
        if (DisparosN2[i] === undefined) continue;
        if (DisparosN2[i].alive == false) {
            DisparosN2.splice(i, 1);
            continue;
        }
        DisparosN2[i].position.add(DisparosN2[i].velocity);
    }

    if (keys["N"]) {
        var bullet = new THREE.Mesh(new THREE.SphereGeometry(0.3, 8, 8), new THREE.MeshBasicMaterial({ color: 0xffffff }));
        bullet.position.set(
            Nave[1].position.x,
            Nave[1].position.y,
            Nave[1].position.z + 3
        );
        bullet.velocity = new THREE.Vector3(
            -Math.sin(camera.rotation.y),
            0,
            Math.cos(camera.rotation.y)
        );
        bullet.alive = true;
        setTimeout(function () {
            bullet.alive = false;
            scene.remove(bullet);
        }, 1000);
        DisparosN2.push(bullet);
        scene.add(bullet);
    }
    //Disparos Nave 2

    Nave[0].translateX(yaw * deltaTime)
    Nave[0].translateZ((forward * deltaTime));
    Nave[1].translateX(yawN2 * deltaTime)
    Nave[1].translateZ((forwardN2 * deltaTime));
    if( plane.position.z <= -206){
        plane.position.z = 306;
    }
    if( plane2.position.z <= -206){
        plane2.position.z = 306;
    }
    plane.position.z = plane.position.z -0.15;
    plane2.position.z = plane2.position.z -0.15;

    if( Ambiental[3].position.z <= -60){
        Ambiental[3].position.z = 260;
    }
    if( Ambiental[4].position.z <= -60){
        Ambiental[4].position.z = 260;
    }
    Ambiental[3].position.z =  Ambiental[3].position.z -0.15;
    Ambiental[4].position.z =  Ambiental[4].position.z -0.15;
    //NAVE MAYOR
    if(comportamiento == 1)
    {
        if(Nave[0].position.x > Enemigos[0].position.x){
            Enemigos[0].translateX(-11 * deltaTime);
        }
        if(Nave[0].position.x < Enemigos[0].position.x){
            Enemigos[0].translateX(11 * deltaTime);
        }
        if(Nave[0].position.x- 0.5 < Enemigos[0].position.x && Nave[0].position.x +0.5 > Enemigos[0].position.x){
            
            comportamiento = 2;
        }
    }
    if(comportamiento == 2)
    {
        if(Nave[1].position.x > Enemigos[0].position.x){
            Enemigos[0].translateX(-11 * deltaTime);
        }
        if(Nave[1].position.x < Enemigos[0].position.x){
            Enemigos[0].translateX(11 * deltaTime);
        }
        if(Nave[1].position.x- 0.5 < Enemigos[0].position.x && Nave[1].position.x +0.5 > Enemigos[0].position.x){
            comportamiento = 1;
        }
    }
    //NAVE MAYOR


    //NAVE KAMIKAZE
    if(kamikaze == 1)
    {
        if(Nave[0].position.x > Enemigos[1].position.x){
            Enemigos[1].translateX(24 * deltaTime);
        }
        if(Nave[0].position.x < Enemigos[1].position.x){
            Enemigos[1].translateX(-24 * deltaTime);
        }
        if(Nave[0].position.z- 0.5 < Enemigos[1].position.z && Nave[0].position.z +0.5 > Enemigos[1].position.z){
            Enemigos[1].position.z = 40;
            kamikaze = 2;
        }
    }
    if(kamikaze == 2)
    {
        if(Nave[1].position.x > Enemigos[1].position.x){
            Enemigos[1].translateX(24 * deltaTime);
        }
        if(Nave[1].position.x < Enemigos[1].position.x){
            Enemigos[1].translateX(-24 * deltaTime);
        }
        if(Nave[1].position.z- 0.5 < Enemigos[1].position.z && Nave[1].position.z +0.5 > Enemigos[1].position.z){
            Enemigos[1].position.z = 40;
            kamikaze = 1;
        }
    }
    Enemigos[1].position.z += -0.15;

    //NAVE KAMIKAZE

    //NAVES LATERALES

    if(lateral1 == 1)
    {
        if(34 <= Enemigos[2].position.x){
            movL1 = -20;

        }
        if(-35 >= Enemigos[2].position.x){
            movL1 = 20;

        }
        if(Nave[0].position.x- 0.5 < Enemigos[2].position.x && Nave[0].position.x + 0.5 > Enemigos[2].position.x){

            lateral1 = 2;
        }
    }
    if(lateral1 == 2)
    {
        if(34 <= Enemigos[2].position.x){
            movL1 = -20;

        }
        if(-35 >= Enemigos[2].position.x){
            movL1 = 20;

        }
        if(Nave[1].position.x- 0.5 < Enemigos[2].position.x && Nave[1].position.x +0.5 > Enemigos[2].position.x){

            lateral1 = 1;
        }
    }
    Enemigos[2].translateX(movL1 * deltaTime);

    if(lateral2 == 1)
    {
        if(34 <= Enemigos[3].position.x){
            movL2= -20;

        }
        if(-35 >= Enemigos[3].position.x){
            movL2 = 20;

        }
        if(Nave[0].position.x- 0.5 < Enemigos[3].position.x && Nave[0].position.x + 0.5 > Enemigos[3].position.x){

            lateral1 = 2;
        }
    }
    if(lateral2 == 2)
    {
        if(34 <= Enemigos[3].position.x){
            movL2 = -20;

        }
        if(-35 >= Enemigos[3].position.x){
            movL2 = 20;

        }
        if(Nave[1].position.x- 0.5 < Enemigos[3].position.x && Nave[1].position.x +0.5 > Enemigos[3].position.x){

            lateral1 = 1;
        }
    }
    Enemigos[3].translateX(movL2 * deltaTime);


    //NAVES LATERALES

    if( cube2.position.z <= -25){
        cube2.position.z = 100;
        cube2.position.x = Math.random() * (34 - -35) + -35 + 34;

    }
    if( cube3.position.z <= -50){
        cube3.position.z = 100;
        cube3.position.x = Math.random() * (34 - -35) + -35 + 34;
    }
    cube2.position.z = cube2.position.z -0.15;
    cube3.position.z = cube3.position.z -0.15;
    renderer.render(scene, camera);

}

function setupScene() {
    const container = document.getElementById('canvas');
    clock = new THREE.Clock();
    scene = new THREE.Scene();
    camera = new THREE.PerspectiveCamera(75, $(container).width() / $(container).height(), 0.1, 100);
    camera.position.y = 30;
    camera.rotation.x = THREE.Math.degToRad(-90);
    camera.rotation.z = THREE.Math.degToRad(-180);

    renderer = new THREE.WebGLRenderer({ precision: "mediump" });
    renderer.setClearColor(new THREE.Color(0, 0, 0));
    renderer.setPixelRatio($(container).width() / $(container).height());
    renderer.setSize($(container).width(), $(container).height());


    var pointLight = new THREE.PointLight(0xffffff);
    pointLight.position.set(0, 10, 20);
    scene.add(pointLight);

    var ambientLight = new THREE.AmbientLight(new THREE.Color(1, 1, 1), 1.0);
    scene.add(ambientLight);

    var directionalLight = new THREE.DirectionalLight(new THREE.Color(1, 1, 0), 0.4);
    directionalLight.position.set(5, 20, 12);
    scene.add(directionalLight);

    var grid = new THREE.GridHelper(50, 10, 0xffffff, 0xffffff);
    grid.position.y = -1;
    scene.add(grid);

    var material = new THREE.MeshLambertMaterial({ color: new THREE.Color(0.5, 0.0, 0.0) });
    var geometry = new THREE.BoxGeometry(1, 1, 1);
    cube = new THREE.Mesh(geometry, material)
    cube.position.y = 0;
    cube.position.z = 20;

    cube2 = cube.clone();
    cube2.position.x = 5;
    

    cube3 = cube.clone();
    cube3.position.x = -15;
    

    //Planos
    const geometryP = new THREE.PlaneGeometry( 256, 256,256,256);
    const texture = new THREE.TextureLoader().load( "../images/Wall_Tiles_Stone_001_basecolor.jpg" );
    texture.wrapS = THREE.RepeatWrapping;
    texture.wrapT = THREE.RepeatWrapping;
    texture.repeat.set( 4, 4 );
    const materialT = new THREE.MeshBasicMaterial( { map: texture } );
    const materialP = new THREE.MeshBasicMaterial( {color: new THREE.Color(0.5, 0.5, 0.5), side: THREE.DoubleSide} );
    plane = new THREE.Mesh( geometryP, materialT );
    plane.position.y = -30;
    plane.position.z = 50;
    plane.rotation.x = THREE.Math.degToRad(-90);
    plane.castShadow = true;
    plane.receiveShadow = true;
    plane2 = plane.clone();
    plane2.position.y=-30;
    plane2.position.z = 306;
    //scene.add( plane );
    //(34,20)(34,-19)(-35,20)(-35,-19)
    //scene.add(plane2);
    //Planos
    comportamiento = 1;
    kamikaze = 1;
    lateral1 =1;
    lateral2 = 1;
    scene.add(cube);
    scene.add(cube2);
    scene.add(cube3);

    container.appendChild(renderer.domElement);
}
/*CARGA DE MODELOS*/

loader.load('/modelos/Nave1/scene.gltf', function (gltf) {
    gltf.scene.scale.set(6, 6, 6);
    Nave[0] = gltf.scene;
    scene.add(Nave[0]);

}, function (xhr) {
    console.log((xhr.loaded / xhr.total * 100) + "% loaded")
}, function (error) {
    console.log('error')
})

loader.load('/modelos/Nave2/scene.gltf', function (gltf) {
    gltf.scene.scale.set(0.08,0.08, 0.08);
    Nave[1] = gltf.scene;
    scene.add(Nave[1]);

}, function (xhr) {
    console.log((xhr.loaded / xhr.total * 100) + "% loaded")
}, function (error) {
    console.log('error')
})

loader.load('/modelos/Imperial/scene.gltf', function (gltf) {
    gltf.scene.scale.set(5,5, 5);
    Enemigos[0] = gltf.scene;
    Enemigos[0].rotation.y = THREE.Math.degToRad(180);
    Enemigos[0].position.z = -43;
    scene.add(Enemigos[0]);

}, function (xhr) {
    console.log((xhr.loaded / xhr.total * 100) + "% loaded")
}, function (error) {
    console.log('error')
})

loader.load('/modelos/City/balloons/scene.gltf', function (gltf) {
    gltf.scene.scale.set(0.5,0.5, 0.5);
    Ambiental[0] = gltf.scene;
    Ambiental[1] = gltf.scene;
    Ambiental[2] = gltf.scene;
    //scene.add(Ambiental[0]);
    //scene.add(Ambiental[1]);
    //scene.add(Ambiental[2]);

}, function (xhr) {
    console.log((xhr.loaded / xhr.total * 100) + "% loaded")
}, function (error) {
    console.log('error')
})

loader.load('/modelos/City/city good/scene.gltf', function (gltf) {
    //gltf.scene.scale.set(0.05,0.05, 0.05);
    gltf.scene.scale.set(0.5,0.5, 0.5);
    Ambiental[3] = gltf.scene;
    Ambiental[4] = Ambiental[3].clone()
    //Ambiental[4].rotation.y = THREE.Math.degToRad(180);
    Ambiental[3].position.y = -50;
    Ambiental[4].position.y = -50;
    //140
    Ambiental[3].position.z = -55;
    Ambiental[4].position.z = 140;
    Ambiental[3].position.x = 105;
    Ambiental[4].position.x = 105;
    
    scene.add(Ambiental[3]);
    scene.add(Ambiental[4]);

}, function (xhr) {
    console.log((xhr.loaded / xhr.total * 100) + "% loaded")
}, function (error) {
    console.log('error')
})

loader.load('/modelos/City/le_millefiori/scene.gltf', function (gltf) {
    gltf.scene.scale.set(0.1,0.1, 0.1);
    Ambiental[5] = gltf.scene;
    Ambiental[6] = gltf.scene;
    Ambiental[5].position.y = -8;
    Ambiental[6].position.y = -8;
    scene.add(Ambiental[5]);
    scene.add(Ambiental[6]);

}, function (xhr) {
    console.log((xhr.loaded / xhr.total * 100) + "% loaded")
}, function (error) {
    console.log('error')
})


loader.load('/gcw/modelos/HMG/scene.gltf', function (gltf) {
    gltf.scene.scale.set(1.5,1.5, 1.5);
    Item[0] = gltf.scene;
    Item[0].rotation.z = THREE.Math.degToRad(90);
    Item[0].rotation.y = THREE.Math.degToRad(90);
    
    

    scene.add( Item[0]);

}, function (xhr) {
    console.log((xhr.loaded / xhr.total * 100) + "% loaded")
}, function (error) {
    console.log('error')
})
loader.load('/gcw/modelos/Kamikaze/scene.gltf', function (gltf) {
    gltf.scene.scale.set(7,7, 7);
    Enemigos[1] = gltf.scene;
    Enemigos[1].position.z = 20;



    scene.add( Enemigos[1]);


}, function (xhr) {
    console.log((xhr.loaded / xhr.total * 100) + "% loaded")
}, function (error) {
    console.log('error')
})


loader.load('/gcw/modelos/Enemigos/scene.gltf', function (gltf) {
    gltf.scene.scale.set(0.8,0.8, 0.8);
    Enemigos[2] = gltf.scene;
    Enemigos[2].position.z = 20;
    Enemigos[2].position.x = 15;
    Enemigos[3] = Enemigos[2].clone();
    Enemigos[3].position.x = -15;


    scene.add( Enemigos[2]);
    scene.add( Enemigos[3]);

}, function (xhr) {
    console.log((xhr.loaded / xhr.total * 100) + "% loaded")
}, function (error) {
    console.log('error')
})


/*FUNCIONES*/

function onKeyDown(event) {
    keys[String.fromCharCode(event.keyCode)] = true;
}
function onKeyUp(event) {
    keys[String.fromCharCode(event.keyCode)] = false;
}

function checkTouching(a, d) {
    let r1 = a.position.x + a.geometry.parameters.width / 2;
    let l1 = a.position.x - a.geometry.parameters.width / 2;
    let f1 = a.position.z - a.geometry.parameters.depth / 2;
    let B1 = a.position.z + a.geometry.parameters.depth / 2;
    let r2 = d.position.x + d.geometry.parameters.width / 2;
    let l2 = d.position.x - d.geometry.parameters.width / 2;
    let f2 = d.position.z - d.geometry.parameters.depth / 2;
    let B2 = d.position.z + d.geometry.parameters.depth / 2;
    if (r1 < l2 || l1 > r2 || f1 > B2 || B1 < f2) {
        return false;
    }
    return true;
}




