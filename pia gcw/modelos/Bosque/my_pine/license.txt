Model Information:
* title:	My_Pine
* source:	https://sketchfab.com/3d-models/my-pine-4d54ee12595746eab344dd36573fe50d
* author:	pashchenko.iryna (https://sketchfab.com/pashchenko.iryna)

Model License:
* license type:	CC-BY-4.0 (http://creativecommons.org/licenses/by/4.0/)
* requirements:	Author must be credited. Commercial use is allowed.

If you use this 3D model in your project be sure to copy paste this credit wherever you share it:
This work is based on "My_Pine" (https://sketchfab.com/3d-models/my-pine-4d54ee12595746eab344dd36573fe50d) by pashchenko.iryna (https://sketchfab.com/pashchenko.iryna) licensed under CC-BY-4.0 (http://creativecommons.org/licenses/by/4.0/)