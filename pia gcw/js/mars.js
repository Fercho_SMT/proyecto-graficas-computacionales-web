import * as THREE from '../three.js-master/build/three.module.js'
import {GLTFLoader} from '../three.js-master/examples/jsm/loaders/GLTFLoader.js'

var scene;
var camera;
var renderer;
var clock;
var deltaTime;	
var keys = {};
var cube;
var Nave = [];
var Disparos = [];
var distanciacolision;
var plane;
var plane2;
const loader = new GLTFLoader()

$(document).ready(function() {
    setupScene();
    document.addEventListener('keydown', onKeyDown);
    document.addEventListener('keyup', onKeyUp);

    render();
});

function render() {
    requestAnimationFrame(render);
    deltaTime = clock.getDelta();	

    var yaw = 0;
    var forward = 0;
    if (keys["A"]) {
        yaw = 25;
    } else if (keys["D"]) {
        yaw = -25;
    }
    if (keys["W"]) {
        forward = 15;
    } else if (keys["S"]) {
        forward = -15;
    }

    for(var i=0; i<Disparos.length; i++){
        distanciacolision = cube.position.distanceToSquared(Disparos[i].position)
        if(distanciacolision <= 0.5 ){
            scene.remove(Disparos[i]);
            console.log("colision")
        }
        if(Disparos[i] === undefined) continue;
        if(Disparos[i].alive == false){
            Disparos.splice(i,1);
            continue;
        }
        Disparos[i].position.add(Disparos[i].velocity);
    }


    if(keys["C"]){
        var bullet = new THREE.Mesh(new THREE.SphereGeometry(0.3,8,8), new THREE.MeshBasicMaterial({color:0xffffff}));
        bullet.position.set(
            Nave[0].position.x,
            Nave[0].position.y,
            Nave[0].position.z + 3
        );
        bullet.velocity = new THREE.Vector3(
            -Math.sin(camera.rotation.y),
            0,
            Math.cos(camera.rotation.y)
        );
        bullet.alive = true;
        setTimeout(function(){
            bullet.alive = false;
            scene.remove(bullet);
        },200);
        Disparos.push(bullet);
        scene.add(bullet);
    }
    Nave[0].translateX(yaw * deltaTime)
    Nave[0].translateZ((forward * deltaTime));
    if( plane.position.z <= -156){
        plane.position.z = 356;
    }
    if( plane2.position.z <= -156){
        plane2.position.z = 356;
    }
    plane.position.z = plane.position.z -0.15;
    plane2.position.z = plane2.position.z -0.15;

    Nave[0].translateX(yaw * deltaTime)
    Nave[0].translateZ((forward * deltaTime));

    renderer.render(scene, camera);
}

function setupScene() {		
    const container = document.getElementById('canvas');
    clock = new THREE.Clock();		
    scene = new THREE.Scene();
    camera = new THREE.PerspectiveCamera(75, $(container).width() / $(container).height(), 0.1, 100);
    camera.position.y = 30;
    camera.rotation.x = THREE.Math.degToRad(-110);
    camera.rotation.z = THREE.Math.degToRad(-180);

    renderer = new THREE.WebGLRenderer( {precision: "mediump" } );
    renderer.setClearColor(new THREE.Color(0, 0, 0));
    renderer.setPixelRatio($(container).width() / $(container).height());
    renderer.setSize($(container).width(), $(container).height());

    var ambientLight = new THREE.AmbientLight(new THREE.Color(1, 1, 1), 1.0);
    scene.add(ambientLight);

    var directionalLight = new THREE.DirectionalLight(new THREE.Color(1, 1, 0), 0.4);
    directionalLight.position.set(0, 0, 1);
    scene.add(directionalLight);

    var grid = new THREE.GridHelper(50, 10, 0xffffff, 0xffffff);
    grid.position.y = -1;
    scene.add(grid);

    var material = new THREE.MeshLambertMaterial({color: new THREE.Color(0.5, 0.0, 0.0)});
    var geometry = new THREE.BoxGeometry(1, 1, 1);
    cube = new THREE.Mesh(geometry, material)
    cube.position.y = 0;
    cube.position.z = 20;
    //Planos
    const geometryP = new THREE.PlaneGeometry( 256, 256,256,256);
    const texture = new THREE.TextureLoader().load( "../images/Marte_.jpg" );
    texture.wrapS = THREE.RepeatWrapping;
    texture.wrapT = THREE.RepeatWrapping;
    texture.repeat.set( 4, 4 );
    const materialT = new THREE.MeshBasicMaterial( { map: texture } );
    const materialP = new THREE.MeshBasicMaterial( {color: new THREE.Color(0.5, 0.5, 0.5), side: THREE.DoubleSide} );
    plane = new THREE.Mesh( geometryP, materialT );
    plane.position.y = -30;
    plane.position.z = 100;
    plane.rotation.x = THREE.Math.degToRad(-90);
    plane.castShadow = true;
    plane.receiveShadow = true;
    plane2 = plane.clone();
    plane2.position.y=-30;
    plane2.position.z = 356;
    scene.add( plane );
    
    scene.add(plane2);
    //Planos

    scene.add(cube);

    container.appendChild(renderer.domElement);
}
/*CARGA DE MODELOS*/

loader.load('/modelos/Nave1/scene.gltf',function(gltf){
    gltf.scene.scale.set(6,6,6);
    Nave[0] = gltf.scene;
    scene.add(Nave[0]);

 }, function(xhr){
     console.log((xhr.loaded/xhr.total * 100) + "% loaded")
 }, function(error){
     console.log('error')
 })

 /*FUNCIONES*/

 function onKeyDown(event) {
    keys[String.fromCharCode(event.keyCode)] = true;
}
function onKeyUp(event) {
    keys[String.fromCharCode(event.keyCode)] = false;
}

function checkTouching(a, d) {
    let r1 = a.position.x + a.geometry.parameters.width / 2;
    let l1 = a.position.x - a.geometry.parameters.width / 2;
    let f1 = a.position.z - a.geometry.parameters.depth / 2;
    let B1 = a.position.z + a.geometry.parameters.depth / 2;
    let r2 = d.position.x + d.geometry.parameters.width / 2;
    let l2 = d.position.x - d.geometry.parameters.width / 2;
    let f2 = d.position.z - d.geometry.parameters.depth / 2;
    let B2 = d.position.z + d.geometry.parameters.depth / 2;
    if ( r1 < l2 || l1 > r2 || f1 > B2 || B1 < f2) {
      return false;
    }
    return true;
  }